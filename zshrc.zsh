#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Customize to your needs...
alias ll="ls -lah"


bindkey '^[OA' history-substring-search-up
bindkey '^[OB' history-substring-search-down

autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /usr/local/bin/terraform terraform

AGNOSTER_PROMPT_SEGMENTS[2]=
RPROMPT='[%D{%H:%M:%S}]'

#source /usr/local/share/zsh-history-substring-search/zsh-history-substring-search.zsh
