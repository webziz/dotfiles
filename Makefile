UNAME=$(shell uname -s)

all: install

install:
	[ -f ~/.zshrc ] || ln -s $(PWD)/zshrc ~/.zshrc
	[ -f ~/.zpreztorc.zsh ] || ln -s $(PWD)/zpreztorc.zsh ~/.zpreztorc.zsh
	[ -f ~/.zshrc.zsh ] || ln -s $(PWD)/zshrc.zsh ~/.zshrc.zsh
	[ -f ~/.gitconfig ] || ln -s $(PWD)/gitconfig ~/.gitconfig
ifeq ($(UNAME),Linux)
	[ -f ~/.os.zsh ] || ln -s $(PWD)/ubuntu/ubuntu.zsh ~/.os.zsh
else 
	[ -f ~/.os.zsh ] || ln -s $(PWD)/mac/mac.zsh ~/.os.zsh
endif

clean:
	rm -f ~/.zshrc
	rm -f ~/.zpreztorc.zsh
	rm -f ~/.zshrc.zsh
	rm -f ~/.gitconfig
	rm -f ~/.os.zsh


