# Dotfiles

`make` does most of the lifting.

Made for Three Machine Setup
1) Ubuntu NUC
2) Macbook Pro and
3) Windows 10

## Git
Move the user info into `.gituser` before make

## Terminal / Zsh

Install:
- Zsh
- [Prezto](https://github.com/sorin-ionescu/prezto)
- [Brew/Linuxbrew](https://brew.sh/)
    - with Brew install `zsh-history-substring-search`
- [Powerline fonts](https://github.com/powerline/fonts)

### Windows terminal

Niceish icons
Copy files from `windows/icons/` to `%LOCALAPPDATA%\packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\RoamingState`

