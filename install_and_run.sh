#!/bin/sh

#Install generic packages
sudo apt install make zsh -y

#Install Prezto 
sudo git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"

#Default into Zsh
sudo chsh -s /bin/zsh

#Run the Make for dotfiles
make
